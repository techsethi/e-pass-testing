require "sinatra"
require "faraday"
require "pry"
Faraday.default_connection = Faraday.new(
  options = { :headers => { :user_agent => "Mozilla/5.0 (compatible; Bingbot/2.0; +http://www.bing.com/bingbot.htm)" } }
)

enable :sessions, :logging, :dump_errors, :raise_errors

get "/" do
  erb :index
end

get "/tor" do
  # @url_to_fetch = "https://check.torproject.org/"
  # @url_to_fetch = "https://verify.epass.jantasamvad.org/epass/app/TEAHUG2/" #Mohit App
  @url_to_fetch = "https://epass.jantasamvad.org/epass/app/S7XUFHA/" # My App
  @fd_response = Faraday.get @url_to_fetch
  erb :tor
end

get "/versions" do
  erb :versions
end

__END__

# Inline templates
@@versions
<%= "Version: #{Faraday::VERSION}, Adapter: #{Faraday::default_adapter}" %>
